class CardsController < ApplicationController

  def index

    @cards = [
      {
        title: 'test1',
        body: 'hello1'
      },
      {
        title: 'test2',
        body: 'hello2'
      },
      {
        title: 'test3',
        body: 'hello3'
      }
    ]

    @cards << @cards*5
    @cards.flatten!

    @cards = Kaminari.paginate_array(@cards).page(params[:page]).per(10)

  end

  def show
  end

end
